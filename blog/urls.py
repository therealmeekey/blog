from django.contrib import admin
from django.urls import path, include
from django.contrib.auth import views as auth_views
from django.contrib.auth.decorators import login_required
from blog import views

urlpatterns = [
    path('', login_required(views.index)),
    path('admin/', admin.site.urls),
    path('blog/', include('helpdesk.urls')),
    path('account/login/', auth_views.LoginView.as_view(), name='login'),
    path('lgt/', auth_views.LogoutView.as_view(), {'next_page': '/account/login/'}, name='logout')
]
